import React from 'react';

const useCurrency = value => {
  const formatter = Intl.NumberFormat('nl-NL', {
    style: 'currency',
    currency: 'EUR'
  });

  return formatter.format(value);
}

export default useCurrency;