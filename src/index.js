import React from 'react';
import ReactDOM from 'react-dom';

// Context
import { GlobalContextProvider } from './context/GlobalContext';
import Firebase, { FirebaseContext } from './firebase/Firebase';

// Components
import App from './components/App.js';

// Stylesheet
import './styles/main.scss';

const rootElement = document.getElementById('root');

ReactDOM.render((
  <GlobalContextProvider>
    <FirebaseContext.Provider value={new Firebase()}>
      <App />
    </FirebaseContext.Provider>
  </GlobalContextProvider>
), rootElement);