import React from 'react';

const Dialog = ({ type, message }) => {
  const classes = 'dialog' + (type ? 'dailog-' + type : '');
    
  return <div className={classes}>{message}</div>;
}

export default Dialog;