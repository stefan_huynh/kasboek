import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

// Pages
import Overview from '../pages/Overview';
import Events from '../pages/Events';
import Savings from '../pages/Savings';

const MainContent = () => {
  return (
    <div className="main-content">
      <Router>
        <Switch>
          <Route path="/(overview|)/" component={Overview} />
          <Route path="/events" component={Events} />
          <Route path="/savings" component={Savings} />
        </Switch>
      </Router>
    </div>
  )
}

export default MainContent;