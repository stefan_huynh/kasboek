import React from 'react';

const UserProfile = props => (
  <div className="user-profile">
    <div className="user-profile__name">
      {props.user.email}
    </div>
    <div className="user-profile__avatar"></div>
  </div>
)

const Navbar = props => {
  return (
    <div className="navbar">
      {props.user && <UserProfile user={props.user} />}
    </div>
  )
}

export default Navbar;