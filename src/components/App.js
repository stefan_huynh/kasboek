import React, { useContext, useEffect, useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

// Routes
import * as routes from '../constants/routes';

// Pages
import Dashboard from '../pages/Dashboard';
import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import NotFound from '../pages/NotFound';

// Context
import GlobalContext from '../context/GlobalContext';
import { FirebaseContext } from '../firebase/Firebase';

const App = () => {  
  const { user, setUser } = useContext(GlobalContext);
  const firebase = useContext(FirebaseContext);

  const [firebaseInit, setFirebaseInit] = useState(false);
  
  useEffect(() => {
    firebase.auth.onAuthStateChanged(authUser => {
      setUser(authUser);
      setFirebaseInit(authUser);
    });
  }, []);
  
  return firebaseInit != false ? (
    <Router>
      <Switch>
        <Route exact path="/signin" component={SignIn} />
        <Route exact path="/join" component={SignUp} />
        <Route path="/" component={Dashboard} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  ) : <div className="loader"></div>
}

export default App;