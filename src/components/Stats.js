import React from 'react';
import * as Icon from 'react-feather';

const Stats = () => {
  return (
    <div className="stats">
      <div className="stat-group">
        <div className="stat-key"><Icon.CreditCard size={17} />Balans</div>
        <div className="stat-value">€ 300,00</div>
      </div>
    </div>
  )
}

export default Stats;