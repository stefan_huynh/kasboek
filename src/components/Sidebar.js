import React from 'react';
import Navigation from '../components/Navigation';
import Stats from '../components/Stats';

const Sidebar = () => {
  return (
    <div className="sidebar">
      <div className="sidebar__logo">
        Casher
      </div>
      <div className="sidebar__content">
        <Navigation />
        <Stats />
      </div>
    </div>
  )
}

export default Sidebar;