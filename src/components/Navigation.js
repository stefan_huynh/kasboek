import React from 'react';
import * as routes from '../constants/routes';

const Navigation = () => {
  const currentPage = window.location.pathname;
  
  return (
    <nav className="navigation">
      <a  
        className={'strong-label' + (currentPage == '/overview'
          ? ' current-page'
          : '')
        }
        href="/overview"
      >
        Overzicht
      </a>
      <a
        className={'strong-label' + (currentPage == '/events'
          ? ' current-page'
          : '')
        }
        href="/events"
      >
        Activiteit
      </a>
      <a
        className={'strong-label' + (currentPage == '/savings'
          ? ' current-page'
          : '')
        }
        href="/savings"
      >
        Sparen
      </a>
    </nav>
  )
}

export default Navigation;