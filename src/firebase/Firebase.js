import React from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';

const FirebaseContext = React.createContext();

const config = {
  apiKey: "AIzaSyARQAf55cwXOpQ9IjOEMwJvpUSPiWHjnkA",
  authDomain: "kasboek-ee03c.firebaseapp.com",
  databaseURL: "https://kasboek-ee03c.firebaseio.com",
  projectId: "kasboek-ee03c",
  storageBucket: "kasboek-ee03c.appspot.com",
  messagingSenderId: "650869824787",
  appId: "1:650869824787:web:fa754010973841fd078ee6"
};

class Firebase {
  constructor() {
    firebase.initializeApp(config);

    this.auth = firebase.auth();
  }
  
  createUser(email, password) {
    return this.auth.createUserWithEmailAndPassword(email, password);
  }

  signIn(email, password) {
    return this.auth.signInWithEmailAndPassword(email, password);
  }

  getCurrentUser() {
    return this.auth.currentUser;
  }

  logout() {
    this.auth.signOut();
  }
}

export default Firebase;
export { FirebaseContext };