import React, { useState, useContext, useEffect } from 'react';
import Dialog from '../components/Dialog';

// Context
import GlobalContext from '../context/GlobalContext';
import { FirebaseContext } from '../firebase/Firebase';

const SignIn = props => {
  const firebase = useContext(FirebaseContext);
  const { user, setUser } = useContext(GlobalContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  
  const handleSubmit = e => {
    e.preventDefault();
    
    firebase.signIn(email, password)
      .then(user => {
        props.history.push('/');
      })
      .catch(error => {
        console.log(error);
        setError(error);
      });
  }

  useEffect(() => {
    if (user) {
      props.history.push('/');
    }
  }, []);
  
  return (
    <div className="auth-modal-wrapper">
      <div className="auth-modal">
        <h1>Inloggen</h1>
        <form onSubmit={handleSubmit}>
          <p>
            <label className="strong-label" htmlFor="email">E-mailadres</label>
            <input
              type="email"
              name="email"
              value={email}
              onChange={e =>  setEmail(e.target.value)}
            />
          </p>
          <p>
            <label className="strong-label" htmlFor="password">Wachtwoord</label>
            <input
              type="password"
              name="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
            />
          </p>
          <p>Heb je nog geen account? <a href="/join">Maak er een aan</a>.</p>
          <p>
            <input
              type="submit"
              value="Inloggen"
            />
          </p>
          {error && <Dialog type="error" message={error.message} />}
        </form>
      </div>
    </div>
  )
}

export default SignIn;