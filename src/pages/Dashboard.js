import React, { useContext, useEffect } from 'react';
import GlobalContext from '../context/GlobalContext';

// Components
import Navbar from '../components/Navbar';
import Sidebar from '../components/Sidebar';
import MainContent from '../components/MainContent';

// Context
import { FirebaseContext } from '../firebase/Firebase';

const Dashboard = props => {
  const firebase = useContext(FirebaseContext);
  const { user } = useContext(GlobalContext);
  
  const logout = () => {
    firebase.logout();
  }
  
  useEffect(() => {
    if (!user) {
      props.history.push('/join');
    }
  }, []);
  
  return (
    <>
      <Navbar user={user} />
      <Sidebar />
      <MainContent />
    </>
  )
}

export default Dashboard;