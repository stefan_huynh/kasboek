import React, { useState, useContext, useEffect } from 'react';
import Dialog from '../components/Dialog';

// Context
import GlobalContext from '../context/GlobalContext';
import { FirebaseContext } from '../firebase/Firebase';

const SignUp = props => {
  const firebase = useContext(FirebaseContext);
  const { user, setUser } = useContext(GlobalContext);

  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  
  const handleSubmit = e => {
    firebase.createUser(email, password)
      .then(authUser => {
        props.history.push('/');
      })
      .catch(error => {
        setError(error);
      });

    e.preventDefault();
  }

  useEffect(() => {
    if (user) {
      props.history.push('/');
    }
  }, []);
  
  return (
    <div className="auth-modal-wrapper">
      <div className="auth-modal">
        <h1>Account aanmaken</h1>
        <form onSubmit={handleSubmit}>
          <p>
            <label className="strong-label" htmlFor="email">Gebruikersnaam</label>
            <input
              type="text"
              name="username"
              value={username}
              onChange={e =>  setUsername(e.target.value)}
            />
          </p>
          <p>
            <label className="strong-label" htmlFor="email">E-mailadres</label>
            <input
              type="email"
              name="email"
              value={email}
              onChange={e =>  setEmail(e.target.value)}
            />
          </p>
          <p>
            <label className="strong-label" htmlFor="password">Wachtwoord</label>
            <input
              type="password"
              name="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
            />
          </p>
          <p>Heb je al een account? <a href="/signin">Log dan hier in</a>.</p>
          <p>
            <input
              type="submit"
              value="Account aanmaken"
            />
          </p>
          {error && <Dialog type="error" message={error.message} />}
        </form>
      </div>
    </div>
  )
}

export default SignUp;