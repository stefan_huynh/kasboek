import React from 'react';
import events from '../api/events.json';
import useCurrency from '../hooks/useCurrency';

const Events = () => {
  return (
    <div className="events card-list">
      <div className="main-content__title">
        <h1>Activiteiten</h1>
      </div>
      {events.map(({type, relation, date, amount, description}) =>
        <div className={`card event event__${type}`}>
          <div className="event__type event__item">
            <span>{type}</span>
          </div>
          <div className="event__relation event__item">
            {relation}
          </div>
          <div className="event__description event__item">
            {description}
          </div>
          <div className="event__amount event__item">
            {type == 'in' ? '+ ' : '- '} {useCurrency(amount)}
          </div>
        </div>
      )}
    </div>
  )
}

export default Events;