import React, { useState } from 'react';

const GlobalContext = React.createContext();

const GlobalContextProvider = props => {
  const [user, setUser] = useState(null);
  const [events, setEvents] = useState();
  
  return (
    <GlobalContext.Provider value={{
      user,
      setUser
    }}>
      {props.children}
    </GlobalContext.Provider>
  )
}

export default GlobalContext;
export { GlobalContextProvider };